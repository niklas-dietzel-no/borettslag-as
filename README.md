# BorettslagAS - Setup

### Backend

#### USE GIT BASH FOR THIS

1. Have MariaDB running on port 3306
2. Create database `CREATE DATABASE borettslag_niklas`
3. Install Python 3.10 or later
4. Change directory `cd backend`
5. Setup virtual environment `py.exe -m venv venv` (If this fails, do `py.exe -m pip install venv` first and try again.)
6. Activate virtual environment `source ./venv/bin/activate`
7. Install packages `pip install -r requirements.txt`
8. Run dev server `DATABASE_CREDENTIALS="myuser:mypassword" SECRET_KEY="mykey" py.exe -m uvicorn main:app` (`SECRET_KEY` can be any string.)


Open browser at `http://localhost:8000/docs` for backend documentation.

### Frontend

1. Install NodeJS 18 LTS
2. Change directory to repository root (directory with `package.json`) `cd borettslagAS`
3. Install packages `npm install`
4. Run dev server `npm run start`

Open browser at `http://localhost:4200` for frontend.
