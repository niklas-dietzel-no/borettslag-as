from typing import Optional, Sequence, Any

import schemas
import models
import security

from sqlalchemy.orm import Session
from sqlalchemy import select, update


def create_user(db: Session, user: schemas.UserCreate) -> models.User:
	db_user = models.User(
		username=user.username,
		email=user.email,
		password_hash=security.hash_password(user.password),
	)

	db.add(db_user)
	db.flush()
	db.commit()
	db.refresh(db_user)
	return db_user


def get_user_by_username(db: Session, username: str) -> Optional[models.User]:
	return db.scalar(select(models.User).where(models.User.username == username))


def delete_user_by_username(db: Session, username: str) -> None:
	user = get_user_by_username(db, username)
	if not user:
		raise RuntimeError(f"User {username} does not exist")

	for comment in user.comments:
		db.delete(comment)

	db.delete(user)

	db.commit()


def patch_user_by_username(db: Session, username: str, patch: schemas.UserPatch) -> models.User:
	user = get_user_by_username(db, username)
	if not user:
		raise RuntimeError(f"User {username} does not exist")

	stmt = update(models.User).where(models.User.username == username)

	dict_patch = dict(patch)

	if patch.password is not None:
		dict_patch |= {"password_hash": security.hash_password(patch.password)}
		dict_patch["password"] = None

	# Source:
	# https://stackoverflow.com/questions/33797126/proper-way-to-remove-keys-in-dictionary-with-none-values-in-python

	# remove None values from dictionary
	data = {k: v for k, v in dict_patch.items() if v is not None}

	db.execute(stmt, data)
	db.commit()

	return get_user_by_username(db, patch.username or username)


def create_comment(db: Session, comment: schemas.InternalCommentCreate) -> models.Comment:
	db_comment = models.Comment(content=comment.content, author_id=comment.author_id)

	db.add(db_comment)
	db.commit()
	db.refresh(db_comment)
	return db_comment


def search_comments(db: Session, query: str) -> Sequence[models.Comment]:
	author = get_user_by_username(db, query)
	if not author:
		return []

	return db.scalars(
		select(models.Comment).where(models.Comment.author_id == author.id)
	).all()


def get_user_by_id(db: Session, user_id: int) -> Optional[models.User]:
	return db.get(models.User, user_id)


def get_all_comments(db: Session) -> Sequence[models.Comment]:
	return db.scalars(select(models.Comment)).all()
