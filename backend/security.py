from datetime import datetime, timedelta
from typing import Optional

import fastapi
from fastapi import status, Depends
from fastapi.security.oauth2 import OAuth2PasswordBearer
from passlib.context import CryptContext

from jose import jwt, JWTError

from sqlalchemy.orm import Session

import environment
import models
import crud
import schemas
import database

SECRET_KEY = environment.secret_key
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_DAYS = 7

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="api/token")
oauth2_scheme_no_auto_error = OAuth2PasswordBearer(tokenUrl="api/token", auto_error=False)

credentials_exception = fastapi.HTTPException(
	status_code=status.HTTP_401_UNAUTHORIZED,
	detail="Could not validate credentials",
	headers={"WWW-Authenticate": "Bearer"},
)


def verify_password(plain_password: str, hashed_password: str):
	return pwd_context.verify(plain_password, hashed_password)


def hash_password(password: str):
	return pwd_context.hash(password)


def authenticate_user(db: Session, username: str, password: str) -> Optional[models.User]:
	user = crud.get_user_by_username(db, username)

	if not user:
		return None
	if not verify_password(password, user.password_hash):
		return None
	return user


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
	to_encode = data.copy()

	if expires_delta:
		expire = datetime.utcnow() + expires_delta
	else:
		expire = datetime.utcnow() + timedelta(days=ACCESS_TOKEN_EXPIRE_DAYS)
	to_encode.update({"exp": expire})

	encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
	return encoded_jwt


def decode_access_token(db: Session, token: str) -> Optional[models.User]:
	try:
		payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
		username: str = payload.get("sub")
		if username is None:
			return None

	except JWTError:
		return None

	user = crud.get_user_by_username(db, username)
	return user


async def get_current_user_or_null(
	db: Session = Depends(database.get_db),
	token: Optional[str] = Depends(oauth2_scheme_no_auto_error)
) -> Optional[models.User]:
	if not token:
		return None

	return decode_access_token(db, token)


async def get_current_user(
	db: Session = Depends(database.get_db),
	token: str = Depends(oauth2_scheme)
) -> models.User:
	user = await get_current_user_or_null(db, token)
	if not user:
		raise credentials_exception
	return user
