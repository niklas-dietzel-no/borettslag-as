#  Copyright (c) 2023 Niklas Dietzel <niklas@dietzel.no>. Licensed under the GNU General Public License v3.
#  See the LICENSE file in the repository root for full licence text.

import sqlalchemy.orm

import environment

engine = sqlalchemy.create_engine(f"mariadb+mariadbconnector://{environment.database_credentials}@localhost/borettslag_niklas")

SessionLocal = sqlalchemy.orm.sessionmaker(autocommit=False, autoflush=False, bind=engine)
BaseModel = sqlalchemy.orm.DeclarativeBase


class Base(BaseModel):
    pass


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
