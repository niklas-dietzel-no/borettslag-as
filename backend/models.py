from datetime import datetime


import database

import sqlalchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import mapped_column, Mapped, relationship
from sqlalchemy.dialects.mysql import TINYTEXT, MEDIUMTEXT


class User(database.Base):
	__tablename__ = "users"

	id: Mapped[int] = mapped_column(sqlalchemy.Integer, primary_key=True, index=True, nullable=False)
	username: Mapped[str] = mapped_column(TINYTEXT, nullable=False, unique=True)
	email: Mapped[str] = mapped_column(TINYTEXT, nullable=False, unique=True)

	password_hash: Mapped[str] = mapped_column(TINYTEXT, nullable=False)

	created_at: Mapped[datetime] = mapped_column(server_default=sqlalchemy.func.now(), nullable=False)

	comments: Mapped[list["Comment"]] = relationship("Comment", back_populates="author", foreign_keys="Comment.author_id")


class Comment(database.Base):
	__tablename__ = "comments"

	id: Mapped[int] = mapped_column(sqlalchemy.Integer, primary_key=True, index=True, nullable=False)
	created_at: Mapped[datetime] = mapped_column(server_default=sqlalchemy.func.now(), nullable=False)

	content: Mapped[str] = mapped_column(MEDIUMTEXT, nullable=False)

	author_id: Mapped[int] = mapped_column(ForeignKey(User.id), nullable=False, index=True)
	author: Mapped[User] = relationship(User, foreign_keys=author_id, back_populates="comments")


# create all tables
database.Base.metadata.create_all(database.engine)
