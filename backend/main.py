from typing import Annotated

from fastapi import FastAPI, status, Depends
from fastapi.security.oauth2 import OAuth2PasswordRequestForm
import fastapi

from sqlalchemy.orm import Session
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware

from datetime import timedelta

import schemas
import crud
import database
import security
import models

origins = [
	"http://localhost:4200",
	"http://localhost:80",
	"http://10.100.4.164:80",
]

middleware = [
	Middleware(CORSMiddleware, allow_origins=origins, allow_methods=["*"], allow_headers=["*"], allow_credentials=True)
]

app = FastAPI(middleware=middleware)


@app.post("/api/users", response_model=schemas.UserBare, status_code=status.HTTP_201_CREATED)
async def create_user(
	user: schemas.UserCreate,
	db: Session = Depends(database.get_db)
):
	return crud.create_user(db, user)


@app.post("/api/token")
async def login(
	form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
	db: Session = Depends(database.get_db)
):
	user = security.authenticate_user(db, form_data.username, form_data.password)

	if not user:
		raise fastapi.HTTPException(
			status_code=fastapi.status.HTTP_401_UNAUTHORIZED,
			detail="Incorrect username or password",
			headers={"WWW-Authenticate": "Bearer"},
		)

	access_token_expires = timedelta(days=security.ACCESS_TOKEN_EXPIRE_DAYS)
	access_token = security.create_access_token(
		data={"sub": user.username}, expires_delta=access_token_expires
	)
	return {"access_token": access_token, "token_type": "bearer"}


@app.get("/api/users/current", response_model=schemas.UserBare)
async def get_current_user(
	user: models.User = Depends(security.get_current_user)
):
	return user


@app.delete("/api/users/current", response_model=None)
async def delete_current_user(
	user: models.User = Depends(security.get_current_user),
	db: Session = Depends(database.get_db)
):
	crud.delete_user_by_username(db, user.username)


@app.patch("/api/users/current", response_model=schemas.UserBare)
async def patch_current_user(
	patch: schemas.UserPatch,
	user: models.User = Depends(security.get_current_user),
	db: Session = Depends(database.get_db)
):
	return crud.patch_user_by_username(db, user.username, patch)


@app.get("/api/users/{user_id}", response_model=schemas.UserBare)
async def get_user_by_id(
	user_id: int,
	db: Session = Depends(database.get_db)
):
	return crud.get_user_by_id(db, user_id)


@app.post("/api/comments", response_model=schemas.Comment)
async def create_comment(
	comment: schemas.CommentCreate,
	user: models.User = Depends(security.get_current_user),
	db: Session = Depends(database.get_db)
):
	internal_comment = schemas.InternalCommentCreate(author_id=user.id, content=comment.content)
	return crud.create_comment(db, internal_comment)


# TODO: return CommentBare or something
@app.get("/api/comments/search", response_model=list[schemas.Comment])
async def search_comments(
	query: str,
	db: Session = Depends(database.get_db)
):
	return crud.search_comments(db, query)


@app.get("/api/comments/all", response_model=list[schemas.Comment])
async def get_all_comments(
	db: Session = Depends(database.get_db)
):
	return crud.get_all_comments(db)
