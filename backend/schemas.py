from datetime import datetime
from typing import Optional

import pydantic
from pydantic import BaseModel


class UserCreate(BaseModel):
	username: str
	email: pydantic.EmailStr

	password: str


class User(BaseModel):
	id: int

	username: str
	email: pydantic.EmailStr
	created_at: datetime

	password_hash: str


class UserBare(BaseModel):
	username: str
	email: str
	id: int


class UserPatch(BaseModel):
	username: Optional[str] = None
	email: Optional[str] = None

	password: Optional[str] = None


class Comment(BaseModel):
	author_id: int
	author: User

	id: int

	content: str
	created_at: datetime


# internal data class. Only used in crud.py, not used in API.
class InternalCommentCreate(BaseModel):
	author_id: int
	content: str


class CommentCreate(BaseModel):
	content: str
