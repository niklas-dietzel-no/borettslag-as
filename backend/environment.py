#  Copyright (c) 2023 Niklas Dietzel <niklas@dietzel.no>. Licensed under the GNU General Public License v3.
#  See the LICENSE file in the repository root for full licence text.

import os

try:
    if os.environ["API_DEV"]:
        print("Running in development environment.")

except KeyError:
    print("Running in production.")

secret_key: str = ""
try:
    secret_key = os.environ["SECRET_KEY"]

except KeyError:
    raise RuntimeError("SECRET_KEY env variable is not set!")

database_credentials: str = ""
try:
    database_credentials = os.environ["DATABASE_CREDENTIALS"]

except KeyError:
    raise RuntimeError("DATABASE_CREDENTIALS env variable is not set!")
