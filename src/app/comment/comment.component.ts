import { Component, Input, OnInit } from '@angular/core';
import { Comment, UserData } from "../api-services/models";
import { UsersService } from "../api-services/users.service";
import { Observable } from "rxjs";
@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnInit {
	@Input({required: true}) comment!: Comment;
	user$: Observable<UserData> | null = null;

	constructor(private usersService: UsersService) {
	}

	ngOnInit(): void {
		this.user$ = this.usersService.userDataById(this.comment.author_id);
	}
}
