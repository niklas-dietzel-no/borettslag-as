import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { CommentsService } from "../api-services/comments.service";
import {Comment} from "../api-services/models";
import { UsersService } from "../api-services/users.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass']
})
export class SearchComponent {
	searchResults: Comment[] = [];

	@ViewChild("queryInput") queryInput!: ElementRef<HTMLInputElement>;
	hasSearched: boolean = false;

	constructor(private commentsService: CommentsService, public usersService: UsersService) {
	}

	onSubmit() {
		this.commentsService.searchComments(this.queryInput.nativeElement.value).subscribe({
			next: comments => {
				this.searchResults = comments;
				this.hasSearched = true;
			}
		});
	}
}
