import { Injectable } from '@angular/core';
import { environment } from "../environment";
import { LoginData, RegisterData, UserData } from "./models";
import { Observable, share, shareReplay } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable({
	providedIn: 'root'
})
export class UsersService {

	constructor(private httpClient: HttpClient) {
	}

	getUserData(): Observable<UserData> {
		return this.httpClient.get<UserData>(`${environment.api_prefix}/users/current`).pipe(share());
	}

	// TODO: error handling when registration fails
	register(data: RegisterData): Observable<UserData> {
		return this.httpClient.post<UserData>(`${environment.api_prefix}/users`, data).pipe(share());
	}

	userDataById(id: number): Observable<UserData> {

		if (this.cachedUserData[id] !== undefined) {
			setTimeout(() => {
				delete this.cachedUserData[id];
			}, 10000);
			return this.cachedUserData[id];
		}

		this.cachedUserData[id] = this.httpClient.get<UserData>(`${environment.api_prefix}/users/${id}`).pipe(shareReplay());
		return this.cachedUserData[id];
	}

	patchUser(data: Partial<UserData>): Observable<UserData> {
		return this.httpClient.patch<UserData>(`${environment.api_prefix}/users/current`, data);
	}

	private cachedUserData: {
		[id: number]: Observable<UserData>
	} = {}

	deleteCurrentUser(): Observable<void> {
		return this.httpClient.delete<void>(`${environment.api_prefix}/users/current`);
	}
}
