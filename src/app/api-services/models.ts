export interface LoginData {
	username: string,
	password: string
}

export interface UserData {
	username: string,
	email: string,
	id: number
}

export interface JWT {
	access_token: string;
	token_type: "bearer";
}


export interface Comment {
	content: string,
	author_id: number,
	created_at: string
}

export interface RegisterData {
	username: string,
	email: string,
	password: string
}
