import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Comment } from "./models";
import { environment } from "../environment";

@Injectable({
	providedIn: 'root'
})
export class CommentsService {

	constructor(private httpClient: HttpClient) {
	}

	searchComments(query: string): Observable<Comment[]> {
		return this.httpClient.get<Comment[]>(`${environment.api_prefix}/comments/search`, {
			params: {
				query: query
			}
		});
	}

	createComment(comment: string): Observable<Comment> {
		return this.httpClient.post<Comment>(`${environment.api_prefix}/comments`, { content: comment });
	}

	getAllComments(): Observable<Comment[]> {
		return this.httpClient.get<Comment[]>(`${environment.api_prefix}/comments/all`);
	}
}
