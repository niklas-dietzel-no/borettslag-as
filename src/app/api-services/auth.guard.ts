
import { CanActivateFn, Router, UrlTree } from '@angular/router';
import { inject } from "@angular/core";
import { AuthService } from "./auth.service";
import { map } from "rxjs";

export const isLoggedInGuard: CanActivateFn = (route, state) => {
	let url = inject(Router).createUrlTree([ "/login" ], {
		queryParams: {
			redirectTo: state.url
		}
	});

	return inject(AuthService).isLoggedIn$.pipe(map((value) => {
		if (value)
			return true;
		return url;
	}));
};
