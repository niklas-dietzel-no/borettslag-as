/*
 * Copyright (c) 2023 Niklas Dietzel <niklas@dietzel.no>. Licensed under the GNU General Public License v3.
 * See the LICENSE file in the repository root for full licence text.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable, ReplaySubject, share, Subject } from "rxjs";
import { JWT, LoginData, UserData } from "./models";
import { environment } from "../environment";
import { TokenService } from "./token.service";
import { UsersService } from "./users.service";

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(private httpClient: HttpClient, private tokenService: TokenService, private usersService: UsersService) {
		this.onLogin$.subscribe({
			next: value => {
				this.onLoggedInLoggedOutSource.next(value);
				this.isLoggedInSource.next(true);
				console.log("Logged in as ", value);
			}
		});

		this.onLogout$.subscribe({
			next: () => {
				this.onLoggedInLoggedOutSource.next(null);
				this.isLoggedInSource.next(false);
				console.log("Logged out");
			}
		});

		this.isLoggedInFromRefresh();
	}

	private isLoggedInFromRefresh(): void {
		this.usersService.getUserData().subscribe({
			next: value => {
				console.log("logged in from refresh: ", value);
				this.onLoginSource.next(value);
			},
			error: err => {
				if (err.status === 401) {
					console.log("not logged in.");
				}
			}
		});
	}

	private onLoginSource: ReplaySubject<UserData> = new ReplaySubject<UserData>(1);
	private onLogoutSource: ReplaySubject<void> = new ReplaySubject<void>(1);
	private onLoggedInLoggedOutSource: BehaviorSubject<UserData | null> = new BehaviorSubject<UserData | null>(null);
	private isLoggedInSource: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

	onLogin$: Observable<UserData> = this.onLoginSource.asObservable();
	onLogout$: Observable<void> = this.onLogoutSource.asObservable();
	onLoggedInLoggedOut$: Observable<UserData | null> = this.onLoggedInLoggedOutSource.asObservable();
	isLoggedIn$: Observable<boolean> = this.isLoggedInSource.asObservable();

	isLoggedIn(): boolean {
		return this.isLoggedInSource.getValue();
	}

	get currentUser(): UserData | null {
		return this.onLoggedInLoggedOutSource.getValue();
	}


	login(data: LoginData): Observable<UserData> {

		let form = new FormData();
		form.set("username", data.username);
		form.set("password", data.password);

		let finishedSubject: Subject<UserData> = new Subject<UserData>();

		this.httpClient.post<JWT>(`${environment.api_prefix}/token`, form, { withCredentials: true }).subscribe({
			next: (jwt: JWT) => {
				this.tokenService.setAccessToken(jwt.access_token);

				this.usersService.getUserData().subscribe({
					next: userData => {
						this.onLoginSource.next(userData);

						finishedSubject.next(userData);
						finishedSubject.complete();
					}
				});
			},

			error: err => {
				finishedSubject.error(err);
			}
		});

		return finishedSubject.asObservable().pipe(share());
	}

	logout(): void {
		this.tokenService.clearToken();
		this.onLogoutSource.next();
	}
}
