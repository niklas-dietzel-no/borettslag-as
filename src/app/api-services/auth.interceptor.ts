import { Injectable } from '@angular/core';
import {
	HttpRequest,
	HttpHandler,
	HttpEvent,
	HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from "./token.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

	constructor(private tokenService: TokenService) {
	}

	intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
		let auth: HttpRequest<unknown>;

		let token = this.tokenService.token();
		if (token) {
			auth = request.clone({
				withCredentials: true,
				headers: request.headers.set("Authorization", `Bearer ${token}`)
			});
		} else {
			auth = request.clone({
				withCredentials: true
			});
		}

		return next.handle(auth);
	}
}
