import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class TokenService {

	private access_token: string | null;

	setAccessToken(token: string): void {
		this.access_token = token;
		localStorage.setItem("access_token", this.access_token);
	}

	clearToken(): void {
		this.access_token = null;
		localStorage.removeItem("access_token");
	}

	token(): string | null {
		return this.access_token;
	}

	constructor() {
		this.access_token = localStorage.getItem("access_token");
	}
}
