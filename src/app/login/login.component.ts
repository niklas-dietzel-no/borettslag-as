import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs";
import { LoginData } from "../api-services/models";
import { AuthService } from "../api-services/auth.service";


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: [ './login.component.sass' ]
})
export class LoginComponent implements OnDestroy {
	loginForm = this.fb.group({
		username: [ '', [ Validators.minLength(3), Validators.required ] ],
		password: [ '', [ Validators.minLength(3), Validators.required ] ],
	});

	redirectSubscription!: Subscription;


	constructor(private fb: FormBuilder, private authService: AuthService, private router: Router, route: ActivatedRoute) {
		this.redirectSubscription = this.authService.isLoggedIn$.subscribe({
			next: value => {
				if (!value)
					return;

				let redirectTo = route.snapshot.queryParamMap.get("redirectTo")
				this.router.navigate([ redirectTo ?? "/dashboard" ]);
			}
		});
	}

	ngOnDestroy(): void {
		this.redirectSubscription.unsubscribe();
	}

	onSubmit(): void {
		console.log(this.loginForm.value);
		this.authService.login(this.loginForm.value as LoginData).subscribe({
			next: value => {
				console.log("Logged in: ", value);
			}
		});
	}
}
