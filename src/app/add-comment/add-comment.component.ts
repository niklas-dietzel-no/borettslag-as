import { Component } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { CommentsService } from "../api-services/comments.service";

@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.sass']
})
export class AddCommentComponent {

	messageForm = this.fb.group({
		message: ["", [Validators.required]]
	});
	constructor(private fb: FormBuilder, private commentsService: CommentsService) {
	}

	onSubmit(): void {
		this.commentsService.createComment(this.messageForm.controls.message.value!).subscribe({
			next: value => {
				this.messageForm.reset();
			}
		});
	}
}
