import { Component } from '@angular/core';
import { AuthService } from "../api-services/auth.service";
import { FormBuilder } from "@angular/forms";
import { UsersService } from "../api-services/users.service";
import { UserData } from "../api-services/models";
import { Router } from "@angular/router";

@Component({
	selector: 'app-user',
	templateUrl: './user.component.html',
	styleUrls: [ './user.component.sass' ]
})
export class UserComponent {

	editDataForm = this.fb.group({
		username: [""],
		password: [""],
		email: [""],
	});

	constructor(public authService: AuthService, private fb: FormBuilder, private usersService: UsersService, private router: Router) {
		let current = this.authService.currentUser;
		if (current === null)
			throw new Error("UserComponent should not be loaded when user is logged out!");

		this.editDataForm.setValue({
			email: current.email,
			username: current.username,
			password: null
		});
	}

	onSubmit(): void {
		console.log(this.editDataForm.value);

		if (this.editDataForm.controls.password.value === "")
			this.editDataForm.controls.password.setValue(null);

		this.usersService.patchUser(this.editDataForm.value as Partial<UserData>).subscribe({
			next: () => {
				this.authService.logout();
				this.router.navigate(["/login"]);
			}
		});
	}

	logOut() {
		this.authService.logout();
		window.location.reload();
	}

	deleteUser() {
		this.usersService.deleteCurrentUser().subscribe({
			next: () => {
				this.logOut();
			}
		});
	}
}
