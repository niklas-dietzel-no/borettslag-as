import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { RegisterComponent } from "./register/register.component";
import { SearchComponent } from "./search/search.component";
import { UserComponent } from "./user/user.component";
import { isLoggedInGuard } from "./api-services/auth.guard";
import { AddCommentComponent } from "./add-comment/add-comment.component";
import { ReportComponent } from "./report/report.component";

const routes: Routes = [
	{ path: "", component: DashboardComponent },
	{ path: "login", component: LoginComponent },
	{ path: "register", component: RegisterComponent },
	{ path: "dashboard", component: DashboardComponent },
	{ path: "search", component: SearchComponent },
	{ path: "user", component: UserComponent, canActivate: [isLoggedInGuard] },
	{ path: "post", component: AddCommentComponent, canActivate: [isLoggedInGuard] },
	{ path: "report", component: ReportComponent },

];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule {
}
