import { Component } from '@angular/core';
import { CommentsService } from "../api-services/comments.service";
import { map, Observable } from "rxjs";
import { Comment, UserData } from "../api-services/models";
import { UsersService } from "../api-services/users.service";

interface CommentWithAuthorObservable extends Comment {
	author: Observable<UserData>;
}

@Component({
	selector: 'app-report',
	templateUrl: './report.component.html',
	styleUrls: [ './report.component.sass' ]
})
export class ReportComponent {
	comments$: Observable<CommentWithAuthorObservable[]>;

	constructor(private commentsService: CommentsService, public usersService: UsersService) {
		this.comments$ = this.commentsService.getAllComments().pipe(map(value => {
			let newValues: CommentWithAuthorObservable[] = [];

			for (const comment of value) {
				let newComment: CommentWithAuthorObservable = {...comment, author: this.usersService.userDataById(comment.author_id)}
				newValues.push(newComment);
			}

			return newValues;
		}));
	}
}
