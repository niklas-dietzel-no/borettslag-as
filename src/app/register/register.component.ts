import { Component, OnDestroy } from '@angular/core';
import { AbstractControl, FormBuilder, ValidationErrors, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { UsersService } from "../api-services/users.service";
import { Router } from "@angular/router";
import { AuthService } from "../api-services/auth.service";
import { LoginData, RegisterData } from "../api-services/models";


function passwordVerifyValidator(
	control: AbstractControl
): ValidationErrors | null {

	let passwordControl = control.get("password");
	let passwordVerifyControl = control.get("passwordVerify");
	if (passwordControl !== null && passwordVerifyControl !== null) {
		const password = passwordControl.value;
		const passwordVerify = passwordVerifyControl.value;
		return (password !== passwordVerify) ? { passwordVerifyError: true } : null
	}

	return null;
}

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: [ './register.component.sass' ]
})
export class RegisterComponent implements OnDestroy {
	registerForm = this.fb.group({
		username: [ '', [ Validators.minLength(3), Validators.required ] ],
		password: [ '', [ Validators.minLength(3), Validators.required ] ],
		passwordVerify: [ '', [ Validators.minLength(3), Validators.required ] ],
		email: ['', [Validators.email]]
	}, { validators: [ passwordVerifyValidator ] });
	private redirectSubscription!: Subscription;

	constructor(private fb: FormBuilder, private usersService: UsersService, private router: Router, private authService: AuthService) {
		this.redirectSubscription = this.authService.isLoggedIn$.subscribe({
			next: value => {
				if (value)
					this.router.navigate([ "/dashboard" ])
			}
		});
	}

	ngOnDestroy(): void {
		this.redirectSubscription.unsubscribe();
	}


	onSubmit(): void {
		let registerData: RegisterData = {
			username: this.registerForm.controls.username.value!,
			password: this.registerForm.controls.password.value!,
			email: this.registerForm.controls.email.value!
		}

		this.usersService.register(registerData).subscribe({
			next: () => {
				this.authService.login(registerData).subscribe();
			}
		});
	}
}
