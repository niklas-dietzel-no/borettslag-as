export interface AppEnvironment {
	api_prefix: string
}

export const environment: AppEnvironment = {
	api_prefix: "http://localhost:8000/api"
}
