import { Component } from '@angular/core';
import { AuthService } from "../api-services/auth.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent {
	constructor(public authService: AuthService) {
	}
}
